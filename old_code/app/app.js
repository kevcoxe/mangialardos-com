var app = angular.module("myApp", ['matchMedia']);
app.controller("myCtrl", ['$scope', 'screenSize', function ($scope, screenSize) {

    $scope.isCollapsed = true;

    // Using dynamic method `on`, which will set the variables initially and then update the variable on window resize
    $scope.desktop = screenSize.on('md, lg', function(match){
        $scope.desktop = match;
    });
    $scope.mobile = screenSize.on('xs, sm', function(match){
        $scope.mobile = match;
    });

  $scope.bread_types = ["Hard Roll", "Soft Roll"];

  $scope.topping_list = ["Lettuce","Tomato","Mayonnaise","Onion","Oil","Vinegar","Hot or Sweet Peppers","Deli Mustard"];

    $scope.subs = {
      "cold": [
        {"name":"Cold Cut", "description":"ham, salami, mortadella, provolone cheese", "price":9.25},
        {"name":"Super Sub", "description":"double meat double cheese of the cold cut", "price":11.25},
        {"name":'"G" Man', "description":"ham, salami, mortadella, pepperoni, fontina cheese, provolone cheese, oregano", "price":9.75},
        {"name":"Big G", "description":"double meat double cheese of the G-man", "price":12.25},
        {"name":"Ham & Cheese", "description":"", "price":9.25},
        {"name":"Turkey & Cheese", "description":"", "price":9.75},
        {"name":"Chicken Salad & Cheese", "description":"", "price":9.75},
        {"name":"Tuna Salad & Cheese", "description":"", "price":9.75},
        {"name":"All Cheese", "description":"", "price":8.75},
        {"name":"Cheese Combo", "description":"", "price":9.25}
      ],
      "hot": [
        {"name":"Pizza Sub", "description":"sause, cheese, pepperoni & oregano, heated up on a roll", "price":9.75},
        {"name":"Meatball & Cheese", "description":"", "price":9.75},
        {"name":"Roast Beef & Cheese", "description":"", "price":9.75},
        {"name":"Corned Beef & Cheese", "description":"", "price":9.75},
        {"name":"Pastrami & Cheese", "description":"", "price":9.75},
        {"name":"Beef Combo", "description":"corn beef, pastrami, roast beef, provolone cheese", "price":11.25}
      ]
    }

}]);
